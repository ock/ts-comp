import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
const app = createApp(App);

import messages from "./components/locale/lang/en-US";
import { Locale} from './components/components'

Locale.use("en-US", messages);


import QUI from './components/index'
import Directive from './directives'


app.use(QUI)
app.use(Directive)
// app.use(Locale)


app.mount('#app');
