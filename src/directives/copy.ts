const copy = {
    
  created(el, { value }, vnode) {
    el.$value = value;
    el.handler = () => {
        // if(!el.$value){
        //     return ;
        // }
        console.log(el.$value)
    }
    el.addEventListener('click', el.handler);
  },
  beforeMount(el, binding, vnode) {
    el.style.background = binding.value
  },
  mounted(el, { value }, vnode) {
  },
  beforeUpdate(a,b,c,prevNode) { //! 第四个参数 prevNode 只在beforeUpdate和updated才有！
    a.style.background = b.value
  },
  updated(el, { value }) {
    // el.removeEventListener('click', el.handler)
  },
  beforeUnmount() {
    // 当指令绑定的元素 的父组件销毁前调用。  <简单讲，指令元素的父组件销毁前调用>
  },
  unmounted() {},// 当指令与元素解除绑定且父组件已销毁时调用。
}

export default copy;