import copy from './copy'

const directive = {
    copy
}


export default {
    install(app) {
        console.log(app);
        Object.keys(directive).forEach((key)=>{
            app.directive(key, directive[key])
        })
    }
}
