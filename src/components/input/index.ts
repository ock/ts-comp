import Input from './index.vue'
import "./style"
import Locale from '../locale';

// console.log(Input)
Input.install = (app) =>{
    app.component(Input.name, Input);
    Locale.install(app);
}

export default Input;