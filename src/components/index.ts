import * as components from "./components";

export * from './components';
export default {
  install(app) {
    Object.keys(components).forEach((key) => {
      app.use(components[key]);
    });
  },
  
  version: "1.23.0",
};


