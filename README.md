# Design of Vue3

   简介

## 特性

- 基于Vite + Vue3 + TypeScipt
-     

## 快速上手

### 1、安装

```
   npm i
```

### 2、启动

```
   npm run dev
```

### 3、新增组件

在/src/components下面创建组件文件夹
包括
- example-comp
  - index.ts  注册组件
  - index.vue 组件
  - README.md 组件文档
  - style.ts  组件样式
  - 

## TODO

- commit规范要求
- router引入 区分文档和demo
- 全局组件开发
- 新组件开发
- 组件包构建